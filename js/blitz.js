function show_word(word, time, target_id) {

  // store placeholder in text
  const text = target.innerHTML;
  // set target text to input word
  target.innerHTML=word;

  setTimeout(function () {
    // restore placeholder
    target.innerHTML=text;
  }, time);
}

/*
  full screen overlay
*/

function openNav() {
  document.getElementById("myNav").style.width = "100%";
  $("#MenuButton").hide()
}

function closeNav() {
  document.getElementById("myNav").style.width = "0%";
  $("#MenuButton").show()
}

/*
  main
*/

document.addEventListener('DOMContentLoaded', function() {

  var slider = document.getElementById("displayTime");
  var inputTime = document.getElementById("inputTime");
  var inputFontSize = document.getElementById("inputFontSize");
  var target = document.getElementById("target");
  var imageIndex = document.getElementById("currentImageIndex");
  var totalImageCount = document.getElementById("totalImageCount");
  var nextImageButton = document.getElementById("nextImageButton");
  var prevImageButton = document.getElementById("prevImageButton");
  let inputTextArray
  let currentWordIndex


    function showCurrentWord() {
      show_word(
        inputTextArray[currentWordIndex-1],
        slider.value, 'target');
    }

    function nextWord() {
      // update index
      if (currentWordIndex < inputTextArray.length) {
        currentWordIndex += 1;
      } else {currentWordIndex = 1;}
      // update display
      imageIndex.innerHTML = currentWordIndex;

      showCurrentWord();
    }

    function previousWord() {
      // update index
      if (currentWordIndex > 1) {
        currentWordIndex -= 1;
      } else {currentWordIndex = inputTextArray.length;}
      // update display
      imageIndex.innerHTML = currentWordIndex;

      showCurrentWord();
    }

  /*
    read text file
  */

  document.getElementById('inputfile')
            .addEventListener('change', function() {

    var fr = new FileReader();

    fr.onload = function() {
        // split input file by lines, assign to inputTextArray
        inputTextArray = fr.result.split(/\r?\n/)
          .filter(Boolean); // remove empty entries from array
        // set currentWordIndex to initial value
        currentWordIndex = 1;
        // set index display to initial value, add total image count
        imageIndex.innerHTML = currentWordIndex;
        totalImageCount.innerHTML = inputTextArray.length;

    }
    // read input file as UTF8
    fr.readAsText(this.files[0]);
  })

  /*
    Buttons & Slider
  */

  // slider value determines time for visibility
  inputTime.value = slider.value;
  // Update time form
  slider.oninput = function() {
    inputTime.value = this.value;
  }

  // Update time slider
  $("#inputTime").change(function() {
    slider.value = inputTime.value;
  });

  // font size input
  $("#inputFontSize").change(function() {
    target.style.fontSize = inputFontSize.value + 'pt';
  });

  // show word on click
  $("#showButton").click(function() {
    showCurrentWord();
  });

  // toggle settings div
  $("#settingsToggle").click(function() {
    $("#settingsContainer").toggle();
  });

  // next word
  $("#nextImageButton").click(function() {
    nextWord();
  });

  // previous image
  $("#prevImageButton").click(function() {
    previousWord();
  });

  /* key presses */

  document.addEventListener('keydown', (event) => {

    if (event.which === 37) { /* left */
      previousWord();
      console.log('previous');
    }

    if (event.which === 39) { /* right */
      nextWord();
      console.log('next');
    }

    if (event.which === 38) { /* up */
      showCurrentWord();
      console.log('show');
    }

  }, false);

});
