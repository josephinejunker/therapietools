/*
  full screen overlay
*/

function openNav() {
  document.getElementById("myNav").style.width = "100%";
  $("#MenuButton").hide()
}

function closeNav() {
  document.getElementById("myNav").style.width = "0%";
  $("#MenuButton").show()
}

document.addEventListener('DOMContentLoaded', function() {

  // get microphone permission from user
  navigator.mediaDevices.getUserMedia({audio: true})
  .then(function(stream) { // apply function to microphone stream
    // for legacy browsers
    const AudioContext = window.AudioContext || window.webkitAudioContext;
    // new audioContext object
    const audioContext = new AudioContext();
    // analyser node
    const analyser = audioContext.createAnalyser();
    // audio source node (microphone)
    const microphone = audioContext.createMediaStreamSource(stream);
    // scriptProcessor node
    const bufferSize = 2048 // other values: 256, 512, 1024, 4096, 8192, 16384
    const javascriptNode = audioContext.createScriptProcessor(bufferSize, 1, 1);

    analyser.smoothingTimeConstant = 0.8;
    analyser.fftSize = 1024;

    // connect nodes
    microphone.connect(analyser); // microphone -> analyser
    analyser.connect(javascriptNode); // analyser -> javascriptNode
    javascriptNode.connect(audioContext.destination); // javascriptNode -> destination

    javascriptNode.onaudioprocess = function() {

        // create array of 8-bit integers
        var array = new Uint8Array(analyser.frequencyBinCount);
        // copy current frequency data to array (scale: 0 to 255)
        analyser.getByteFrequencyData(array);
        console.log(array);
        // initialize variable for accumulated levels
        var values = 0;

        var length = array.length;
        // add all values in array to variable
        for (var i = 0; i < length; i++) {
          values += (array[i]);
        }

        // get average value by dividing values / length
        var average = values / length;

      // return average value
      //console.log(Math.round((average / 255)*100));
      visualize((average / 255)*100);
    }
    })
    .catch(function(err) {
      /* handle the error */
  });

  /*
    visualize
  */

  // get visualizer element
  const vis = document.getElementById('visualizer');

  function visualize(level) {
    if (level >= 15 && level <= 30) {
      // good level
      document.body.style.background = 'green';
      vis.innerHTML = 'gut!';
      console.log('good');
    }
    else if (level < 1) {
      // no input
      vis.innerHTML = 'nix';
      document.body.style.background = 'gray';
    }
    else if (level > 1 && level < 15) {
      // not loud enough
      vis.innerHTML = 'zu leise';
      document.body.style.background = 'orange';
    }
    else {
      // too loud
      vis.innerHTML = 'zu laut';
      document.body.style.background = 'red';
    }
  };
})
